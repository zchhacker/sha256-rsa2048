#include <stdio.h>
#include <string.h>
#include "Algorithm\Hash\Hash_256\Sha_256.h"
#include "Algorithm\Rsa\Rsa2048.h"
#include "Convert\ConvertDataType.h"

#define SHA256_ENABLE (1)
#if (SHA256_ENABLE)
#define SHA256_USE_GENERATE (0)
#define RSA2048_ENABLE (1)
#endif

#if (RSA2048_ENABLE)
#define RSA2048_USE_DefPubKey (1)
#endif

#if (SHA256_ENABLE)
STATIC Sha256Calc Sha256CalcValue;

void SecM_Sha256CalcInit(void)
{
  (void)Sha256Calc_init(&Sha256CalcValue);
}

uint8 *SecM_Sha256CalcCalculate(const uint8 *dp, uint32 dl)
{
  Sha256Calc_calculate(&Sha256CalcValue, dp, dl);
  return Sha256CalcValue.Value;
}
#endif

void main(void)
{
#if (SHA256_ENABLE)
  /* hash */
  char array_sha256[SHA256_LENGTH]={0};

#if (SHA256_USE_GENERATE)
  SecM_Sha256CalcInit();

  uint8 SduDataPtr[2] = {0};
  SduDataPtr[0] = 0xFF; // hex value, and char ending with \0
  SduDataPtr[1] = 0xFF;
  printf("Plaintext is:");
  TRACE(SduDataPtr,2);

  SecM_Sha256CalcCalculate((uint8 *)SduDataPtr, 2);
#endif

  printf("Sha256 is:");
#if (SHA256_USE_GENERATE)
  memcpy(array_sha256,Sha256CalcValue.Value,SHA256_LENGTH);
  TRACE(array_sha256,SHA256_LENGTH);
#else
  char *input_sha256 = "80BCEDA048D16BCB4EE9A2F3DE0856EDC59F20C32C1FF4DBEA6EA18EE437826D";
  string2hex(input_sha256, array_sha256);
  TRACE(array_sha256,SHA256_LENGTH);
#endif

#endif

#if (RSA2048_ENABLE)
  int ret = 0;
#if (!RSA2048_USE_DefPubKey)
  /* public key */
  char *input_rsa2048_pubkey = "";
  char array_rsa2048_pubkey[RSA_PUBLIC_KEY_LENGTH];
  string2hex(input_rsa2048_pubkey, array_rsa2048_pubkey);
#endif

  /* signture */
  char *input_signture = "0C68C2893A0FA6F3D983D6E98BA03A0FC9BBF9E6DE4648B1EDBFD7ED98123A91AEC616EA96BE814E546D88309648CD74462A06B5DB013A5399586347647816174B98012800ACF83309BAD940545540B12E890994BF66FF9B2325B81BA8BFDE205CC870C0DEECA766DA0E3E39B4E2FBA877DF010107139A2DED304C5BCD8A9062C49D94F22B62E83862A834B28354911F26374F0E2B3CAFB9ACEF23230E0B80316E985207A53A25D733B61E23032241F9DB7F14A1C7787B477661D584E05D12BCDECA3BC27E2330684BE21812F9219AE964F1D7137BA15F22B30F1DC3476180C7060FBE506401DA2AD566847B17121202B7AC6D2C4476AA96D625EFB82275A7C6";
  char array_signture[0x100u]={0};
  string2hex(input_signture, array_signture);

  /* Algorithm */
  Rsa_verifyInit();
#if (RSA2048_USE_DefPubKey)
  Rsa_MemcpyPublicKey(PublicbufDcDefault,RSA_PUBLIC_KEY_LENGTH);
#else
  Rsa_MemcpyPublicKey(input_rsa2048_pubkey,RSA_PUBLIC_KEY_LENGTH);
#endif

  ret = Rsa_verify(array_sha256, SHA256_LENGTH, array_signture, 0x100u);
  printf("ret %d\n",ret);
#endif
}
