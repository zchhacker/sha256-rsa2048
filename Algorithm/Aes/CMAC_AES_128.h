
#ifndef CMAC_AES_128_H
#define CMAC_AES_128_H
#ifndef CMAC_AES_128_C
#define CMAC_AES_128_EXPT extern
#else
#define CMAC_AES_128_EXPT
#endif
/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/** <VERSION>   <DATE>      <AUTHOR>     <REVISION LOG>*/
/*=======[ Includes ]=========================================================*/
#include "std_types.h"
/*=======[ Exported define ]==================================================*/
/*=======[ Exported types ]===================================================*/
/*=======[ Exported variables ]===============================================*/
/*=======[ Exported functions ]===============================================*/

CMAC_AES_128_EXPT void CMAC_AES_128 ( uint8 *key, uint8 *input, uint32 length, uint8 *mac );

#endif
/*=======[E N D   O F   F I L E]==============================================*/
