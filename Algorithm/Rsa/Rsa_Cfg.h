/*============================================================================*/
/*
 *
 *  @file       <Rsa_Cfg.h>
 *  @brief      <Rsa Configuration file>
 *
 *
 */
/*============================================================================*/

#ifndef _RSA_CFG_H
#define _RSA_CFG_H

#include "..\..\Other\Std_Types.h"

/* Public Key operate */
#define RSA_PUBLIC_KEY_LENGTH    		256u

/*Default public key parameter configuration*/
extern const uint8 PublicbufDcDefault[RSA_PUBLIC_KEY_LENGTH];

#endif /* CFG_RSA_CFG_H */
