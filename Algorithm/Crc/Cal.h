/*============================================================================*/
/*
 *  @file       <Cal.h>
 *  @brief      <Calculate the CRC >
 *
 *  @author     <Gary Chen>
 *  @date       <2018-11-09>
 *
 */
/*============================================================================*/
#ifndef CAL_H
#define CAL_H

/*=======[I N C L U D E S]====================================================*/
#include "Std_Types.h"
#include "Cal_Cfg.h"

/*=======[E X T E R N A L   D A T A]==========================================*/

/*=======[T Y P E   D E F I N I T I O N S]====================================*/
/** type for Crc value */
#if (CAL_CRC32 == CAL_METHOD)
typedef uint32 SecM_CRCType;
#else
typedef uint16 SecM_CRCType;
#endif

/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
extern void Cal_CrcInit(SecM_CRCType *curCrc);

extern void Cal_CrcCal(SecM_CRCType *curCrc, const uint8 *buf, const uint32 size);

extern void Cal_CrcFinalize(SecM_CRCType *curCrc);

#endif/* end of CAL_H */

/*=======[E N D   O F   F I L E]==============================================*/
