/*============================================================================*/
/*
 *  @file       <SecM_Cfg.c>
 *  @brief      <Config File for Security Module >
 *              Config the SecM_EcuMask
 *
 *
 */
/*============================================================================*/

#include "SecM_Cfg.h"

/* Default MASK value for 27 key calc. */
const uint8 SecM_EcuMask[SECM_SECCONSTKEY_LENGTH] =
{
	0x00,0x2F,0xA2,0x0D,0x58,0x47,0x84,0xA2,
	0x20,0x61,0x05,0xD6,0x30,0xA2,0xB0,0x4D
};



