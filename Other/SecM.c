/*============================================================================*/
/*
 *  @file       <SecM.c>
 *  @brief      <Security set>
 *              <seed and key generate for UDS>
 *  
 *  @author     <Gary Chen>
 *  @date       <2018-11-09>
 *
 */
/*============================================================================*/

/*=======[I N C L U D E S]====================================================*/
#include "SecM.h"
#include "Appl.h"

/*=======[E X T E R N A L   D A T A]==========================================*/

/*=======[I N T E R N A L   D A T A]==========================================*/
STATIC Sha256Calc Sha256CalcValue;
/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
STATIC SecM_StatusType SecM_ProcessCrc(SecM_CRCParamType *crcParam,
                                       uint32 Address,
                                       uint32 Length);

STATIC SecM_StatusType SecM_Verify_SegmentList(uint8 VBTSegmentNo,
											   const SecM_VerifyParamType *verifyParam,
											   FL_SegmentInfoType* VBFInfo);
/*=======[F U N C T I O N   I M P L E M E N T A T I O N S]====================*/

/******************************************************************************/
/**
 * @brief               <compute CRC>
 * 
 * <process CRC compute,include init,compute and finish> .
 * Service ID   :       <NONE>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Reentrant>
 * @param[in]           <NONE>
 * @param[out]          <NONE>
 * @param[in/out]       <crcParam (IN/OUT)>
 * @return              <SecM_StatusType>    
 */
/******************************************************************************/
SecM_StatusType SecM_ComputeCRC(SecM_CRCParamType *crcParam)
{
    SecM_StatusType ret = SECM_OK;

    switch (crcParam->crcState)
    {
    case SECM_CRC_INIT:
        /* CRC value initialize */
        Cal_CrcInit(&crcParam->currentCRC);
        break;

    case SECM_CRC_COMPUTE:
        /* CRC value compute */
        Cal_CrcCal(&crcParam->currentCRC,
                    crcParam->crcSourceBuffer,
                   (uint32)crcParam->crcByteCount);
        break;

    case SECM_CRC_FINALIZE:
        /* CRC value finish */
        Cal_CrcFinalize(&crcParam->currentCRC);
        break;

    default:
        ret = SECM_NOT_OK;
        break;
    }
    return ret;
}

/******************************************************************************/
/**
 * @brief               <verificate CRC value>
 *
 * <verificate if transfered CRC is equal to computed CRC> .
 * @param[in]           <NONE>
 * @param[out]          <NONE>
 * @param[in/out]       <verifyParam (IN/OUT)>
 * @return              <SecM_StatusType>
 */
/******************************************************************************/
SecM_StatusType SecM_Verification(SecM_VerifyParamType *verifyParam)
{
    SecM_StatusType   ret = SECM_OK;
    SecM_CRCParamType crcParam;
    uint8             segmentIndex;
    SecM_CRCType      transferedCrc;

    if (NULL_PTR == verifyParam->segmentList)
	{
		/* if segment list is NULL, verification is failed */
		ret = SECM_NOT_OK;
	}
	else
	{
		/* initialize CRC */
		crcParam.currentCRC = 0U;
		crcParam.crcState = (uint8)SECM_CRC_INIT;
		ret = SecM_ComputeCRC(&crcParam);
	}

	for (segmentIndex = 0u;
		 (segmentIndex < verifyParam->segmentList->nrOfSegments) && (SECM_OK == ret);
		 segmentIndex++)
	{
		/* compute each segment CRC */
		ret = SecM_ProcessCrc(&crcParam,
							  verifyParam->segmentList->segmentInfo[segmentIndex].address,
							  verifyParam->segmentList->segmentInfo[segmentIndex].length);
	}

	if (SECM_OK == ret)
	{
		/* finish compute CRC */
		crcParam.crcState = (uint8)SECM_CRC_FINALIZE;
		ret = SecM_ComputeCRC(&crcParam);
	}
	else
	{
		/* empty */
	}

    if (SECM_OK == ret)
    {
        /* get CRC transfered from client */
#if (CAL_CRC32 == CAL_METHOD)
    	/* CRC32 */
    	transferedCrc = Appl_Get4Byte(verifyParam->verificationData);
#else
        /* CRC16 */
        transferedCrc = ((SecM_CRCType)verifyParam->verificationData[0] << (uint8)8);
        transferedCrc += (SecM_CRCType)verifyParam->verificationData[1];
#endif

        /* compare CRC */
        if (transferedCrc != crcParam.currentCRC)
        {
            ret = SECM_NOT_OK;

        }
        else
        {
            /* empty */
        }
    }
    else
    {
        /* empty */
    }

    return ret;
}

/******************************************************************************/
/**
 * @brief               <process CRC compute>
 *
 * <CRC compute> .
 * @param[in]           <Address (IN), Length (IN)>
 * @param[out]          <NONE>
 * @param[in/out]       <crcParam (IN/OUT)>
 * @return              <SecM_StatusType>
 */
/******************************************************************************/
STATIC SecM_StatusType SecM_ProcessCrc(SecM_CRCParamType *crcParam,
                                       uint32 Address, uint32 Length)
{
    SecM_StatusType ret = SECM_OK;
    uint32          readLength = 0U;

    /* set CRC compute step */
    crcParam->crcState = (uint8)SECM_CRC_COMPUTE;

    while ((Length > 0U) && (SECM_OK == ret))
    {
        /* read maximum length is SECM_CRC_BUFFER_LEN */
        if (Length > SECM_CRC_BUFFER_LEN)
        {
            readLength = SECM_CRC_BUFFER_LEN;
        }
        else
        {
            readLength = Length;
        }

        /* the readLength will be restricted in lien 321- 325 */
        crcParam->crcByteCount = (uint16)readLength;
        crcParam->crcSourceBuffer = (uint8 *)Address;
        /* compute CRC */
        ret = SecM_ComputeCRC(crcParam);

        Length  -= readLength;
        Address += readLength;

        /* update watch dog */
        Appl_UpdateTriggerCondition();
    }

    return ret;
}


/******************************************************************************/
/**
 * @brief               <verificate RSA value>
 *
 * <verificate if transfered RSA is equal to computed RSA> .
 * Service ID   :       <NONE>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Reentrant>
 * @param[in]           <NONE>
 * @param[out]          <NONE>
 * @param[in/out]       <verifyParam (IN/OUT)>
 * @return              <SecM_StatusType>
 */
/******************************************************************************/
SecM_StatusType SecM_Verification_RSA(const SecM_VerifyParamType *verifyParam)
{
	SecM_StatusType   	ret = SECM_VERIFICATION_PASSED;
	boolean			    rsaRet;
	uint8             	segmentIndex;
	boolean 		  	processContinue = FALSE;

	Sha256Calc			sha256CalcValue;
	uint8 				VBTSegmentNo = 0u;
	FL_SegmentInfoType  * VBFInfo;

	uint32 				length;
	uint8				tempbuf[4u];
	uint32				tempAddr;

	uint32				mbfAddress;

	if ((NULL_PTR == verifyParam->segmentList)
	 || (0u == verifyParam->segmentList->nrOfSegments))
	{
		/* if segment list is NULL, verification is failed */
		ret = SECM_NO_DATA_DOWNLOADED_AT_ALL_NOTHING_TO_VERIFY;
		processContinue = FALSE;
	}
	else
	{
#if (APPL_BOOT_MODE == APPL_BOOT_MODE_PBL)
		mbfAddress = FL_SBL_MBT_ADDRESS;
#elif (APPL_BOOT_MODE == APPL_BOOT_MODE_SBL)
		uint8 currentBlockIndex;
		currentBlockIndex = FL_Get_CurrentBlockIndex();
		mbfAddress = FL_BlkInfo[currentBlockIndex].LBT_address;
#endif
		/* find & get the VBT data-block*/
		for (segmentIndex = (uint8)0;
			segmentIndex < verifyParam->segmentList->nrOfSegments;
			segmentIndex++)
		{
			if(mbfAddress == verifyParam->segmentList->segmentInfo[segmentIndex].address)
			{
				/* get the index of the VBT block in the segment */
				VBTSegmentNo = segmentIndex;
				processContinue = TRUE;
				break;
			}
			else
			{
				/*if not find the VBT info, that means can't to verify,(nothing to verify)*/
				ret = SECM_THE_SIGNED_DATA_COULD_NOT_BE_AUTHENTICATED;
				processContinue = FALSE;
			}
		}
	}
	/*if the VBT is valid, and the segment check is right,
	 */
	/*check the complete of the VBT */
	if(processContinue == TRUE)
	{
		if(verifyParam->segmentList->segmentInfo[VBTSegmentNo].length != ((((uint32)verifyParam->segmentList->nrOfSegments - 1u) * 40U) + 4U))
		{
			/* processContinue is FALSE */
			ret = SECM_INVALIDFORMAT_OR_LENGTH_OF_THE_VERIFICATION_BLOCK_TABLE;
			processContinue = FALSE;
		}
		/*The address or hash values of the downloaded data blocks does not match the expected values*/
		else
		{
			/* get the start address of first data-block info except the VBT data-block*/
			VBFInfo = (FL_SegmentInfoType*)(mbfAddress + 4u);

			/*process verify-param SegmentList NrOfSegments data*/
			ret = SecM_Verify_SegmentList(VBTSegmentNo, verifyParam, VBFInfo);
			if(ret != SECM_VERIFICATION_PASSED)
			{
				processContinue = FALSE;
			}
		}
	}

	/*
	 *
	 * then verify the sign��
	 * The signed data could be authenticate
	 */
	if(processContinue == TRUE)
	{
		/* initialize Hash */
		(void)Sha256Calc_init(&sha256CalcValue);

		/* fixme:  because the address of VBT is 0x8000000,
		 * so should change 0xAxxxxxxx to 0x8xxxxxxx */
#if (APPL_BOOT_MODE == APPL_BOOT_MODE_PBL)
		tempAddr = verifyParam->segmentList->segmentInfo[VBTSegmentNo].address;
#else
		tempAddr = verifyParam->segmentList->segmentInfo[VBTSegmentNo].address & 0xDFFFFFFFu;
#endif

		/* get the hash value of the address of the VBT data-block */
		Appl_Set4Byte(tempAddr, tempbuf);
		(void)Sha256Calc_calculate(&sha256CalcValue, tempbuf, 4u);

		/* get the hash value of the length of the VBT data-block */
		Appl_Set4Byte(verifyParam->segmentList->segmentInfo[VBTSegmentNo].length, tempbuf);
		(void)Sha256Calc_calculate(&sha256CalcValue, tempbuf, 4u);

		/* get the length of data in VBT block */
		length = verifyParam->segmentList->segmentInfo[VBTSegmentNo].length;

		/* get the hash value of the data of VBT data-block */
		(void)Sha256Calc_calculate(&sha256CalcValue, (uint8*)mbfAddress, length);

		/* verify the sw_signature*/
		rsaRet = Rsa_verify(sha256CalcValue.Value, SHA256_LENGTH, verifyParam->verificationData, 0x100u);
		if(rsaRet == SECM_NOT_OK)
		{
			ret = SECM_THE_BLANK_CHECK_FAILED;
		}
		else
		{
			ret = SECM_VERIFICATION_PASSED;
		}
	}

	return ret;
}

/******************************************************************************/
/**
 * @brief               <process>
 *
 * <process verifyparam SegmentList NrOfSegments data> .
 * Service ID   :       <NONE>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Reentrant>
 * @param[in]           <VBTSegmentNo(IN) verifyParam(IN) VBFInfo(IN)>
 * @param[out]          <NONE>
 * @param[in/out]       <NONE>
 * @return              <ret >
 */
/******************************************************************************/
STATIC SecM_StatusType SecM_Verify_SegmentList(uint8 VBTSegmentNo, const SecM_VerifyParamType *verifyParam, FL_SegmentInfoType* VBFInfo)
{
	SecM_StatusType   	ret = SECM_VERIFICATION_PASSED;
	uint8			  	segmentIndex;
	boolean 		  	processContinue = FALSE;
	uint8				addrbuf[4u];
	uint8				lenbuf[4u];
	uint32				tempAddr;
	for (segmentIndex = 0u;
		 segmentIndex < verifyParam->segmentList->nrOfSegments;
		 segmentIndex++)
	{
		if( segmentIndex == VBTSegmentNo)
		{
			/* jump the VBT Data-block(segment) */
			continue;
		}
		else
		{
			/* fixme:  because the address of MBT is 0x8000000,
			 * so should change 0xAxxxxxxx to 0x8xxxxxxx */
#if (APPL_BOOT_MODE == APPL_BOOT_MODE_PBL)
			tempAddr = verifyParam->segmentList->segmentInfo[segmentIndex].address;
#else
			tempAddr = verifyParam->segmentList->segmentInfo[segmentIndex].address & 0xDFFFFFFFu;
#endif
			/*because the data store mode is difference, so need to change one */
			Appl_Set4Byte(tempAddr, addrbuf);
			Appl_Set4Byte(verifyParam->segmentList->segmentInfo[segmentIndex].length,  lenbuf);

			/* compare the address of vbfInfo */
			processContinue = Appl_Memcompare((uint8*)&(VBFInfo->address) , addrbuf, 4u);

			if(processContinue == TRUE)
			{
				/* compare the length of vbfInfo */
				processContinue = Appl_Memcompare((uint8*)&(VBFInfo->length),  lenbuf,	4u);
			}

			if(processContinue == TRUE)
			{
				/* compare the sha256Info of vbfInfo, 32bytes */
				processContinue =Appl_Memcompare(VBFInfo->sha256Info,
												 verifyParam->segmentList->segmentInfo[segmentIndex].sha256Info,
												 (uint32)SHA256_LENGTH);
			}

			if(processContinue == TRUE)
			{
				/*if is the same, to check the next segment
				 *else return is not OK.
				 **/
				VBFInfo++;
			}
			else
			{
				ret = SECM_THE_ADDR_OR_HASH_OF_THE_DOWN_DATA_BLOCKS_DONOT_MATCH_THE_EXPECTED_VALUSE;
				break;
			}
		}
	}
	return ret;
}

/******************************************************************************/
/**
 * @brief               <Sha256 Calc init>
 *
 * <Sha256 Calc init> .
 * Service ID   :       <NONE>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Reentrant>
 * @param[in]           <NONE>
 * @param[out]          <NONE>
 * @param[in/out]       <NONE)>
 * @return              <NONE>
 */
/******************************************************************************/
void SecM_Sha256CalcInit(void)
{
	(void)Sha256Calc_init(&Sha256CalcValue);
}

/******************************************************************************/
/**
 * @brief               <Sha256 Calc Calculate>
 *
 * <Sha256 Calc Calculate> .
 * Service ID   :       <NONE>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Reentrant>
 * @param[in]           <NONE>
 * @param[out]          <NONE>
 * @param[in/out]       <NONE)>
 * @return              <NONE>
 */
/******************************************************************************/
uint8 * SecM_Sha256CalcCalculate(const uint8* dp, uint32 dl)
{
	Sha256Calc_calculate(&Sha256CalcValue, dp, dl);
	return Sha256CalcValue.Value;
}
/******************************************************************************/
/**
 * @brief               <Get Sha256 Calc Calculate Value>
 *
 * <Get Sha256 Calc Calculate Value> .
 * Service ID   :       <NONE>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Reentrant>
 * @param[in]           <NONE>
 * @param[out]          <NONE>
 * @param[in/out]       <NONE)>
 * @return              <NONE>
 */
/******************************************************************************/
uint8 * SecM_GetSha256CalcCalculateValue(void)
{
	return Sha256CalcValue.Value;
}

/*=======[E N D   O F   F I L E]==============================================*/
