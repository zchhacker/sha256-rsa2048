/*============================================================================*/
/*
 *  @file       <SecM.h>
 *  @brief      <Security set>
 *              <seed and key generate for UDS>
 *  
 *  @author     <Gary Chen>
 *  @date       <2018-11-09>
 *
 */
/*============================================================================*/
#ifndef SECM_H
#define SECM_H

/*=======[I N C L U D E S]====================================================*/
#include "std_types.h"
#include "secm_cfg.h"
#include "fl.h"
#include "Cal.h"
#include "Sha_256.h"
#include "Rsa2048.h"

/*=======[M A C R O S]========================================================*/

#define SECM_OK            (uint8)0x00U
#define SECM_NOT_OK    	   (uint8)0x01U


#define SECM_VERIFICATION_PASSED 															0x00U

#define SECM_THE_SIGNED_DATA_COULD_NOT_BE_AUTHENTICATED 									0x02U
#define SECM_THE_PUBLIC_KEY_INTEGRITY_CHECK_FAILED 											0x03U
#define SECM_INVALIDFORMAT_OR_LENGTH_OF_THE_VERIFICATION_BLOCK_TABLE 						0x04U
#define SECM_THE_ADDR_OR_HASH_OF_THE_DOWN_DATA_BLOCKS_DONOT_MATCH_THE_EXPECTED_VALUSE		0x05U
#define SECM_THE_BLANK_CHECK_FAILED 														0x06U
#define SECM_NO_DATA_DOWNLOADED_AT_ALL_NOTHING_TO_VERIFY 									0x07U
#define SECM_READ_ERROR_DURING_HASH_CALCULATION_OVER_MEMORY_CONTENT 						0x08U
#define SECM_ADDITIONAL_PROCESSOR_FAILED_VERIFICATION 										0x09U
#define SECM_ERROR_STORING_VALIDITY_STATUS_INFORMATION	 									0x0AU
#define SECM_CERTIFICATE_VERIFICATION_FAILED 												0x0BU
#define SECM_USER_DEFINABLE 																0x0CU


/** CRC step */
#define SECM_CRC_INIT	    0x00u
#define SECM_CRC_COMPUTE	0x01u
#define SECM_CRC_FINALIZE	0x02u

#if (CAL_CRC32 == CAL_METHOD)
#define SECM_CRC_LENGTH     0x04u
#else
#define SECM_CRC_LENGTH     0x02u
#endif

/*=======[T Y P E   D E F I N I T I O N S]====================================*/
/** return type for SecM module */
typedef uint8 SecM_StatusType;

/** type for Seed */
typedef uint32 SecM_WordType;

/** struct type for Seed */
typedef uint32 SecM_SeedType;

/** type for Key */
typedef SecM_WordType SecM_KeyType;

/** struct type for Crc */
typedef struct
{
    /* current CRC value */
    SecM_CRCType currentCRC;

    /* CRC step */
    uint8 crcState;

    /* CRC buffer point */
    const uint8 *crcSourceBuffer;

    /* CRC length */
    uint16 crcByteCount;

} SecM_CRCParamType;

/** struct type for verify parameter list */
typedef struct
{
    /* segment list for block */
    FL_SegmentListType *segmentList;

    /* Crc value transfered by UDS */
    const uint8 *verificationData;

    /* Crc value title */
    SecM_CRCType crcTotle;

} SecM_VerifyParamType;
/*=======[E X T E R N A L   D A T A]==========================================*/

/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

extern SecM_StatusType SecM_ComputeCRC(SecM_CRCParamType *crcParam);

extern SecM_StatusType SecM_Verification(SecM_VerifyParamType *verifyParam);
extern SecM_StatusType SecM_Verification_RSA(const SecM_VerifyParamType *verifyParam);

extern void   	SecM_Sha256CalcInit(void);
extern uint8*   SecM_Sha256CalcCalculate(const uint8* dp, uint32 dl );
extern uint8* 	SecM_GetSha256CalcCalculateValue(void);
#endif /* end of SECM_H */

/*=======[E N D   O F   F I L E]==============================================*/
