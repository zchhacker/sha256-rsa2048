/*============================================================================*/
/*
 *  @file       <SecM_Cfg.h>
 *  @brief      <Config File for Security Module >
 *              Config the SECM_ECU_KEY 
 *  
 *  @author     <Gary Chen>
 *  @date       <2018-05-09>
 *
 */
/*============================================================================*/
#ifndef SECM_CFG_H
#define SECM_CFG_H

#include "std_types.h"

/*=======[M A C R O S]========================================================*/

#define SECM_SECCONSTKEY_LENGTH    		16U

extern const uint8 SecM_EcuMask[SECM_SECCONSTKEY_LENGTH];

/* CRC check length once for SecM_ProcessCrc. */
#define SECM_CRC_BUFFER_LEN 			0x1000u


#endif/* end of SECM_CFG_H */

/*=======[E N D   O F   F I L E]==============================================*/
